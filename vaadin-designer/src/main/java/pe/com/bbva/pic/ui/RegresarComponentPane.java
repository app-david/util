package pe.com.bbva.pic.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CssLayout;

public abstract class RegresarComponentPane extends CssLayout implements ClickListener{
	
	private static final long serialVersionUID = 3142848773742640591L;
	
	protected Button    btnRetornar;
	
	public RegresarComponentPane() {
		super();
		this.setWidth("100%");
		this.setHeight("100%");
		this.setStyleName("util-margen-izquierdo-uno");
		this.cargarPropiedadadComponente();
	}
	
	protected void cargarPropiedadadComponente() {
		this.btnRetornar = new Button();
		this.btnRetornar.addClickListener(this);
		this.addComponent(btnRetornar);
	}

}
