package pe.com.bbva.pic.util;


public class ClienteUtil {
	
	public static class TIPO_CLIENTES {		
		public final static String PERSONA_RENIEC		=	"PERSONA_RENIEC";
		public final static String PERSONA_PERSON 		=	"PERSONA_PERSON";
		public final static String PERSONA_DASHBORAD 	= 	"PERSONA_DASHBOARD";
	}
	
	public static class TIPO_VISTA {		
		public final static String VISTA_CLIENTE_TOC		=	"VISTA_CLIENTE_TOC";
	}
	
	public static class TITULOS {		
		public final static String SEXO		=	"Sexo";
		public final static String NOMBRE 	=	"Nombres y apellidos";
		public final static String FECHA_NACIMIENTO 	=	"Fecha de nacimiento";
		public final static String FECHA_VENCIMIENTO 	=	"Fecha de vencimiento";
		public final static String ESTADO_CIVIL 	=	"Estado civil";
		public final static String NACIONALIDAD 	=	"Nacionalidad";
		public final static String PAIS_RESIDENCIA 	=	"País residencia";
		public final static String PAIS_RESIDENCIA_FISCAL 	=	"País residencia fiscal";
		public final static String OCUPACION 	=	"Ocupación";
	}
	
	public static class PAGES {		
		public final static String PAGE_DATOS_CLIENTE		=	"PAGE_DATOS_CLIENTE";
		public final static String PAGE_DATOS_TARJETA 	    =	"PAGE_DATOS_TARJETA";
		public final static String PAGE_DATOS_CONFIRMACION  =	"PAGE_DATOS_CONFIRMACION";
	}
	
	public static class ESTILOS {
		public final static String COMPONENTE_FLOTAR_IZQUIERDA = "util-flotar_izquierda";
		public final static String ENUNCIADO_ESTILO = "tituloEnunciado";
		public final static String contenedorCirclePage = "contenedorCirclePage";
	}
	
	public static class TIPO_OPERACION_TEXTO {
		public final static String AGREGAR_TEXTO_INFORMATIVO = "AGREGAR_TEXTO";
		public final static String REMOVER_TEXTO_INFORMATIVO = "REMOVER_TEXTO";
	}
	
	
	
	public static class TIPO_INFORMACION_FORMULARIO_CLIENTE {
		public final static String CODIGO_INFO_CORREO = "COD_INFO_EMAIL";
		public final static String CODIGO_INFO_DIRECCION = "COD_INFO_DIRECCION";
		public final static String INFO_CORREO = "Este correo se registrara en caso el cliente lo solicite";
		public final static String CODIGO_INFO_TELEFONO_SEGURO = "COD_INFO_TELEFONO_SEGURO";
		public final static String INFO_TELEFONO_SEGURO = "El teléfono seleccionado será afiliado a la clave SMS en caso el cliente lo solicite.";
	}
	
	public static class TIPO_INFORMACION_FORMULARIO_DATO_CONTACTO_CLIENTE {
		public final static String CODIGO_INFO_DATO_CONTACTO = "COD_INFO_DATO_CONTACTO";
		public final static String INFO_TELEFONO_AGREGAR = "El número que vas a ingresar se registrará en la ficha del cliente.";
		public final static String INFO_TELEFONO_EDITAR = "Al editar este número también lo estarás editando en la ficha del cliente.";
		public final static String INFO_TELEFONO_ELIMINAR = "Este número será eliminado de la ficha del cliente.";
		public final static String INFO_CORREO_AGREGAR = "El correo que vas a ingresar se registrará en la ficha del cliente.";
		public final static String INFO_CORREO_EDITAR = "Al editar este correo también lo estarás editando en la ﬁcha del cliente.";
		public final static String INFO_CORRREO_ELIMINAR  = "Este correo será retirado de la ficha cliente.";
		public final static String INFO_MSJ_DATO_CONTACTO_ELIMINAR = "¿Deseas eliminar {0} <span style='font-weight: bold;'> {1} </span>?";
		public final static String INFO_DIRECCION_AGREGAR   = "La dirección que vas a ingresar se registrará en la ficha del cliente.";
		public final static String INFO_DIRECCION_EDITAR    = "Al editar esta dirección también lo estarás editando en la ﬁcha del cliente.";
		public final static String INFO_DIRECCION_ELIMINAR  = "Esta dirección será retirada de la ficha cliente.";
	}
	
	public static String formatoTextoCuenta(String texto01, String texto02){
		return "<div  style='width:100%;'>"+
					"<label style='width:100%;'>"+ 
					"<span style='color:#0079C1; font-weight:bold;'>"+texto01+":</span>&nbsp;&nbsp;&nbsp;"+
						texto02+"</label>"+
			   "</div>";
	}
	
	public static String numberCircle(String numeroPagina){
		return "<div  class='circlePage'><div style='margin-top: -6px; font-size: 11px; font-weight: bold;'>"+numeroPagina+ "</div></div>";
	}
	
	
	public static String montoMaximoLinea(String monto){
		return  "<div>"+
			        "<div style='float:left;' class='tituloEnunciado' >Monto máximo de línea: </div><div style='float:right;font-weight: bold;'>" +
					monto+
				"</div></div>";
	}
	
	
}
