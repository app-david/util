package pe.com.bbva.pic;

import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.UI;

import pe.com.bbva.pic.ui.AtencionDevolucionesPresenter;

@SpringUI
@Theme("bbva")
@Widgetset("pe.com.bbva.pic.ui.PicAppWidgetset")
@PreserveOnRefresh
public class MyUI extends UI{

	private static final long serialVersionUID = 1L;

	@Override
	protected void init(VaadinRequest request) {
		AtencionDevolucionesPresenter cssSeguros = new AtencionDevolucionesPresenter();
		//SolicitudAtencionCuentasDesign cssCuentas = new SolicitudAtencionCuentasDesign();
		
		//Agregar como contenido el Design o Presenter de tipo componente vaadin
		setContent(cssSeguros);
//		setContent(cssCuentas);
	}

}
