package pe.com.bbva.pic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaadinDesignerApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaadinDesignerApplication.class, args);
	}
}
