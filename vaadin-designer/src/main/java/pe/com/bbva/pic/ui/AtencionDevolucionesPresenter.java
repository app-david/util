package pe.com.bbva.pic.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AtencionDevolucionesPresenter extends AtencionDevolucionesDesign{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private RegresarComponentPresenter regresarComponentPresenter;
	
	public AtencionDevolucionesPresenter() {
		super();
//		this.fichaPersona = fichaPersona;
		cargarComponentesExternos();
//		cargarComboTiposSolicitud();
	}
	
	private void cargarComponentesExternos() {
		regresarComponentPresenter = new RegresarComponentPresenter(null, null, "Cancelar");
		hlyContenedorRegresar.addComponent(regresarComponentPresenter);
	}
	
//	private void cargarComboTiposSolicitud() throws NegocioException {
//		resuelveComponent.cargarComboTiposSolicitud(super.cmbTipoSolicitud);
//	}
}
