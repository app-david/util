package pe.com.bbva.pic.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class SolicitudAtencionSegurosDesign extends CssLayout{

	private static final long serialVersionUID = 1L;
	
	protected Label lblNumContrato;
	protected ComboBox cmbNumContrato;
	protected Button btnSelecMovimientos;
	protected TextField txtMovSelec;
		
	protected Label lblTituloAbono;
	protected Label lblContratoAbono;
	protected ComboBox cmbSelecContrato;
	protected Label lblTipo;
	protected ComboBox cmbSelecTipo;
	protected Label lblDescripcion;
	protected TextField txtDescripcion;
	protected Label lblMonedaAbono;
	protected ComboBox cmbMonedaAbono;
	protected Label lblMonto;
	protected TextField txtMonto;
	
	protected Label lblTituloCargo;
	protected Label lblTituloDif;
	protected Label lbTipoSeguro;
	protected ComboBox cmbTipoSeguro;
	protected Label lblMonedaCargo;
	protected ComboBox cmbMonedaCargo;
	protected Label lblMontoTotal;
	protected TextField txtMontoTotal;
	protected Label lblCentroContable;
	protected TextField txtCentroContable;
	
	public SolicitudAtencionSegurosDesign() {
		Design.read(this);
		lblNumContrato.setValue("N�mero de contrato");
		cmbNumContrato.setInputPrompt("Seleccionar");
		cmbNumContrato.setNullSelectionAllowed(false);
		btnSelecMovimientos.setCaption("Seleccionar movimientos");
		
		lblTituloAbono.setValue("Abono del Cliente");
		lblContratoAbono.setValue("Contrato de abono");
		cmbSelecContrato.setInputPrompt("Seleccionar");
		cmbSelecContrato.setNullSelectionAllowed(false);
		lblTipo.setValue("Tipo");
		cmbSelecTipo.setInputPrompt("Seleccionar");
		cmbSelecTipo.setNullSelectionAllowed(false);
		lblDescripcion.setValue("Descripci�n");
		lblMonedaAbono.setValue("Moneda");
		cmbMonedaAbono.setInputPrompt("Seleccionar");
		cmbMonedaAbono.setNullSelectionAllowed(false);
		lblMonto.setValue("Monto");
		
		lblTituloCargo.setValue("Cargo a la empresa de seguros");
		lblTituloDif.setValue("Diferencia de tipo de cambio asumido por");
		lbTipoSeguro.setValue("Tipo de seguro");
		cmbTipoSeguro.setInputPrompt("Seleccionar");
		cmbTipoSeguro.setNullSelectionAllowed(false);
		lblMonedaCargo.setValue("Moneda");
		cmbMonedaCargo.setInputPrompt("Seleccionar");
		cmbMonedaCargo.setNullSelectionAllowed(false);
		lblMontoTotal.setValue("Monto total");
		lblCentroContable.setValue("Centro contable");
	}
	
}
