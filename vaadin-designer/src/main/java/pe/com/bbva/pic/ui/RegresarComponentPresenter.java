package pe.com.bbva.pic.ui;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;

@org.springframework.stereotype.Component
public class RegresarComponentPresenter extends RegresarComponentPane {
	
	private static final long serialVersionUID = 3142848773742640591L;
	
	private Component contenedorVisible;
	private Component contenedorNoVisible;
	private String captionButton;
	
	public RegresarComponentPresenter() {
	}
	
	public RegresarComponentPresenter(Component contenedorVisible, Component contenedorNoVisible, String captionButton) {
		super();
		this.captionButton       = captionButton;
		this.contenedorNoVisible = contenedorNoVisible;
		this.contenedorVisible   = contenedorVisible;
		this.setCaptionButton();
	}
	
	public void setCaptionButton() {
		this.btnRetornar.setCaption(captionButton);
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		if(event.getButton().equals(btnRetornar)){
			regresarVista();
		}
		
	}
	
	public void regresarVista() {
		this.contenedorVisible.setVisible(true);
		this.contenedorNoVisible.setVisible(false);
	}

}
