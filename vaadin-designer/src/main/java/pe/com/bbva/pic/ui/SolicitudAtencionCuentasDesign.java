package pe.com.bbva.pic.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.declarative.Design;

@DesignRoot
public class SolicitudAtencionCuentasDesign extends CssLayout{

	private static final long serialVersionUID = 1L;
	
	protected Label lblNumCuenta;
	protected ComboBox cmbNumCuenta;
	protected Button btnSelecMovimientos;
	protected TextField txtMovSelec;
		
	protected Label lblCuentaAbono;
	protected ComboBox cmbSelecAbono;
	protected Label lblTipo;
	protected TextField txtSelecTipo;
	protected Label lblDescripcion;
	protected TextField txtDescripcion;
	
	protected Label lblCentroContable;
	protected ComboBox cmbCentroContable;
	protected Label lblMoneda;
	protected ComboBox cmbMoneda;
	protected Label lblMonto;
	protected TextField txtMonto;
	
	protected Label lblModoAbono;
	protected ComboBox cmbModoAbono;
	
	public SolicitudAtencionCuentasDesign() {
		Design.read(this);
		lblNumCuenta.setValue("N�mero de cuenta");
		cmbNumCuenta.setInputPrompt("Seleccionar");
		cmbNumCuenta.setNullSelectionAllowed(false);
		btnSelecMovimientos.setCaption("Seleccionar movimientos");
		
		lblCuentaAbono.setValue("Cuenta de abono");
		cmbSelecAbono.setInputPrompt("Seleccionar");
		cmbSelecAbono.setNullSelectionAllowed(false);
		lblTipo.setValue("Tipo");
		lblDescripcion.setValue("Descripci�n");
		
		lblCentroContable.setValue("Centro contable");
		cmbCentroContable.setInputPrompt("Seleccionar");
		cmbCentroContable.setNullSelectionAllowed(false);
		lblMoneda.setValue("Moneda");
		cmbMoneda.setInputPrompt("Seleccionar");
		cmbMoneda.setNullSelectionAllowed(false);
		lblMonto.setValue("Monto total");
		
		lblModoAbono.setValue("Modo de abono");
		cmbModoAbono.setInputPrompt("Seleccionar");
		cmbModoAbono.setNullSelectionAllowed(false);
	}
	
}
