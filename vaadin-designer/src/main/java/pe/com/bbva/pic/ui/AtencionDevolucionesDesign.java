package pe.com.bbva.pic.ui;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.declarative.Design;

import pe.com.bbva.pic.util.ClienteUtil;

@DesignRoot
public class AtencionDevolucionesDesign extends CssLayout{

	private static final long serialVersionUID = 1L;

	protected HorizontalLayout hlyContenedorTitulo;
		protected Label lblTituloPrincipal;
		protected Label lblDescripcionPrincipal;
		protected Label lblPasos;
		protected Label lblPagina1;
		protected Label lblPagina2;
		protected HorizontalLayout hlyContenedorRegresar;
	
	protected CssLayout hlyContenedorSelecSolicitud;
		protected Label lblCmbTipoSolicitud;
		protected ComboBox cmbTipoSolicitud;
		
	protected Accordion accordion;
	
	public AtencionDevolucionesDesign() {
		Design.read(this);
		lblTituloPrincipal.setValue("Atención de devoluciones");
		lblDescripcionPrincipal.setValue("Completa los datos del formulario. Recuerda que puedes agregar varias solicitudes a esta atención.");
		lblPasos.setValue("Pasos: ");
		lblCmbTipoSolicitud.setValue("Solicito devolución de:");
		cmbTipoSolicitud.setInputPrompt("Seleccionar");
		cmbTipoSolicitud.setNullSelectionAllowed(false);
		lblPagina1.setContentMode(ContentMode.HTML);
		lblPagina2.setContentMode(ContentMode.HTML);
		inicializarPagina();
		
		SolicitudAtencionSegurosDesign seguros = new SolicitudAtencionSegurosDesign();
		
		SolicitudAtencionTarjetasDesign tarjetas = new SolicitudAtencionTarjetasDesign();
		
		SolicitudAtencionCuentasDesign cuentas = new SolicitudAtencionCuentasDesign();
		
		Tab acordionSolicitud = accordion.addTab(seguros);
		acordionSolicitud.setCaption("Solicitud 01");
		acordionSolicitud.setStyleName("estiloAcordion");
		acordionSolicitud.setClosable(true);
		
		Tab acordionSolicitud2 = accordion.addTab(tarjetas);
		acordionSolicitud2.setCaption("Solicitud 02");
		acordionSolicitud2.setStyleName("estiloAcordion");
		acordionSolicitud2.setClosable(true);
		
		Tab acordionSolicitud3 = accordion.addTab(cuentas);
		acordionSolicitud3.setCaption("Solicitud 03");
		acordionSolicitud3.setStyleName("estiloAcordion");
		acordionSolicitud3.setClosable(true);
		
		accordion.setTabCaptionsAsHtml(true);
	}
	
	private void inicializarPagina() {
		lblPagina1.setValue(ClienteUtil.numberCircle("1"));
		lblPagina1.addStyleName("paginaSeleccionada");
		lblPagina2.setValue(ClienteUtil.numberCircle("2"));
	}
}
